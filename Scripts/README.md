## Carpeta de código para alojar los scripts del proyecto

Esta carpeta aloja todo el código realizado para el caso de estudio. Tiene dos(2) subcarpetas, que pertenecen a los scripts realizados por el usuario y el analista.

* Scripts_Usuarios
* Scripts_Analista
