![dane_logo](https://gitlab.com/icg_projects/my_first_rep/-/raw/main/Figuras/dane_logo.png)

```
git remote add origin https://gitlab.com/icg_projects/my_first_rep.git

```

# Título piloto

## Introducción
## Objetivo
## Alcance
## Descripción de la información
## Cronograma de actividades
## Organización del proyecto

# Contenido

1. Marco teórico 
2. Métodos  
3. Resultados
4. Discusión
5. Conclusiones
6. Recomendaciones

## Marco teórico

Descripción del marco conceptual de las TI implementadas (Conceptos relevantes relacionados al piloto)

## Métodos  

Diagrama del flujo de datos en producción (pipeline).

## Resultados 

Descripción de los principales resultados obtenidos en el entorno de la nube.

## Discusión 

Discusión de los resultados obtenidos (tématico).

## Conclusiones

Conclusiones principales del piloto.

## Recomendaciones

Recomenaciones para futuros casos de estudios relacionados.

## Autores

Usuarios del piloto (usuarios temáticos y analista encargado)
